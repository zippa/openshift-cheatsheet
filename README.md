<!-- omit in toc -->
# **_OpenShift command cheat sheet_**

- [**Login/User management**](#loginuser-management)
  - [Login](#login)
  - [Whoami](#whoami)
- [**Project management**](#project-management)
- [**Resource management**](#resource-management)
  - [List nodes](#list-nodes)
  - [List pod](#list-pod)
  - [Inspect user/group permissions](#inspect-usergroup-permissions)
  - [Inspect imagestreams](#inspect-imagestreams)
  - [Get specific item](#get-specific-item)
  - [Creating a ROOT service accounts](#creating-a-root-service-accounts)
  - [Define nodeSelector in Project](#define-nodeselector-in-project)
  - [Disable defaultNodeSelector in Project](#disable-defaultnodeselector-in-project)
  - [Change Route Timeout](#change-route-timeout)
  - [Delete Pod](#delete-pod)
  - [Delete Pod in Terminating state](#delete-pod-in-terminating-state)
  - [Delete app](#delete-app)
- [**Cluster management**](#cluster-management)
  - [Add users to project](#add-users-to-project)
- [**Additional resource management**](#additional-resource-management)
  - [Define livenessProve and readinessProve](#define-livenessprove-and-readinessprove)
  - [Define resource requests and limits](#define-resource-requests-and-limits)
- [**Operational commands**](#operational-commands)
  - [Get pod log](#get-pod-log)
  - [Get CrashLoop pod log](#get-crashloop-pod-log)
  - [Rsh into pod](#rsh-into-pod)
  - [Exec single command in pod](#exec-single-command-in-pod)
  - [Remote shell](#remote-shell)
  - [Debug pod](#debug-pod)
- [**Build / Deploy**](#build--deploy)
  - [Turn off/on DC triggers](#turn-offon-dc-triggers)
- [**Snippets**](#snippets)
  - [Run as root (anyuid)](#run-as-root-anyuid)
  - [Run as root (anyuid) for every pod in project](#run-as-root-anyuid-for-every-pod-in-project)
  - [Docker push to ocp internal registry](#docker-push-to-ocp-internal-registry)
  - [Create a configmap file and mount as a volume on DC](#create-a-configmap-file-and-mount-as-a-volume-on-dc)
  - [Create Cron Job](#create-cron-job)
  - [Delete pod Failed/Evicted/Pending](#delete-pod-failedevictedpending)
  - [Reset Node](#reset-node)

## **Login/User management**

| Command   | Info                                 |
| --------- | ------------------------------------ |
| oc login  | authenticate to an openshift cluster |
| oc logout | end the current session              |
| oc whoami | show the current user context        |

### Login 
```bash
# Root  ( run command in Master )
oc login -u "system:admin"

# User
oc login -u myuser https://openshift.example.com
Authentication required for https://openshift.example.com
Username: myuser
Password:
```

### Whoami 
```bash
oc whoami
```

## **Project management**

| Command         | Info                                                         |
| --------------- | ------------------------------------------------------------ |
| oc project      | show the current project context                             |
| oc get projects | show all project current login has access to                 |
| oc status show  | overview of current project resources                        |
| oc new-project  | create a new project in Openshift and change to that context |

## **Resource management**

| Command      | Info                                                                                  |
| ------------ | ------------------------------------------------------------------------------------- |
| oc new-app   | create a new application from from source code, containerimage, or OpenShift template |
| oc new-build | create a new build configuration from source code                                     |
| oc label     | add/update/remove labels from an Openshift resource                                   |
| oc annotate  | add/update/remove annotations from an Openshift resource                              |
| oc create    | create a new resource from filename or stdin                                          |
| oc get       | retrieve a resource (use -o for additional output options)                            |
| oc replace   | replace an existing resource from filename or stdin                                   |
| oc delete    | delete a resource                                                                     |
| oc edit      | modify a resource from text editor                                                    |
| oc describe  | retrieve a resource with details                                                      |

### List nodes
```bash
oc get nodes
```

### List pod
```bash
# single namespace
oc get pod -n <namespace>

# all namespaces with details
oc get pod -o wide --all-namespaces

# pod names (for scripting)
oc get pod -o name
```

### Inspect user/group permissions
```bash
oc get rolebinding -o wide -n gitea
oc get rolebinding -o wide --all-namespaces
```

### Inspect imagestreams
```bash
oc get is -n openshift
oc describe is php -n openshift
oc export -n openshift isimage php@42c4a9072f
```

### Get specific item
```bash
# using Go template (for scripting)
oc get dc docker-registry --template='{{range .spec.template.spec.containers}}{{.image}}{{end}}'
oc get service docker-registry --template='{{.spec.clusterIP}}'
oc get pod docker-registry-2-xxx --template='{{.status.podIP}}'
```

### Creating a ROOT service accounts
```
oc create serviceaccount <name>
oc adm policy add-cluster-role-to-user cluster-admin -z <name>
oc sa get-token <name>
```

### Define nodeSelector in Project
```
oc annotate namespace default openshift.io/node-selector=region=infra
```

### Disable defaultNodeSelector in Project
```
oc annotate namespace default openshift.io/node-selector=""
```

### Change Route Timeout
```
oc annotate route jenkins --overwrite haproxy.router.openshift.io/timeout=180s
```

### Delete Pod
```
oc delete pod
```

### Delete Pod in Terminating state
```
oc delete pod <pod_name> --grace-period=0 --force
```

### Delete app
```bash
# Delete Services 
oc delete services -l app=ruby-ex
service "ruby-ex" deleted

# Delete all by identify
oc delete all -l app=ruby-ex
buildconfig "ruby-ex" deleted
imagestream "ruby-22-centos7" deleted
imagestream "ruby-ex" deleted
deploymentconfig "ruby-ex" deleted

# Delete all project 
oc delete project myproject
```

## **Cluster management**

| Command                      | Info                                                                                |
| ---------------------------- | ----------------------------------------------------------------------------------- |
| oc adm                       | administrative functions for an openshift cluster                                   |
| oc adm router/registry       | install a router or registry                                                        |
| oc adm policy                | manage role/scc to user/group bindings, as well as additional policy administration |
| oc adm diagnostics           | run tests/validation against a cluster                                              |
| oc adm cordon/uncordon/drain | unschedule/schedule/drain a node                                                    |
| oc adm groups                | manage groups                                                                       |
| oc adm top                   | show usage statistics of resources                                                  |

### Add users to project
```
# We can add additional users to our project by default, 
# since self-provisioners get the "admin" role for 
# any project they create

$ oc adm policy add-role-to-user edit anotheruser
```

## **Additional resource management**

| Command             | Info                                                                                     |
| ------------------- | ---------------------------------------------------------------------------------------- |
| oc patch            | Update fields for a resource with JSON or YAML segments                                  |
| oc extract          | get configmaps or secrets and save to disk                                               |
| oc set              | Modify miscellaneous application resources                                               |
| oc set probe        | Add a readiness/liveness probe on pod template/deployment configuration                  |
| oc set volumes      | Manage volume types on a pod template/deployment configuration                           |
| oc set build-hook   | Set a script/command to execute as part of the build process                             |
| oc set build-secret | set a secret to be included as part of the build process                                 |
| oc set env          | set environment variables on a pod template/deployment configuration/build configuration |
| oc set image        | update the image for deployment configurations/daemonsets                                |
| oc set triggers     | set triggers for deployment configurations/build configurations                          |

### Define livenessProve and readinessProve
```
oc set probe dc/nginx --readiness --get-url=http://:8080/healthz --initial-delay-seconds=10
oc set probe dc/nginx --liveness --get-url=http://:8080/healthz --initial-delay-seconds=10
```

### Define resource requests and limits
```
oc set resources deployment nginx --limits=cpu=200m,memory=512Mi --requests=cpu=100m,memory=256Mi
```

## **Operational commands**

| Command  | Info                                                                                        |
| -------- | ------------------------------------------------------------------------------------------- |
| oc logs  | retrieve the logs for a resource (build configurations,deployment configurations, and pods) |
| oc rsh   | remote shell into a container                                                               |
| oc rsync | copy files to or from a container                                                           |
| oc exec  | execute a command in a container                                                            |
| oc run   | create a deployment configuration from image                                                |
| oc idle  | scale resources to zero replicas                                                            |
| oc debug | launch a new instance of a pod for debugging

### Get pod log
```bash
oc logs <pod_name> --timestamps

# follow 
oc logs -f <pod_name> --timestamps
```

### Get CrashLoop pod log
```
oc logs -p <pod_name> --timestamps
```

### Rsh into pod
```
oc rsh <pod_name>
```

### Exec single command in pod
```
oc exec <pod_name> $COMMAND
```

### Remote shell
```
oc rsh <pod_name>
```

### Debug pod
```bash
oc debug dc $DC_NAME
oc debug dc $DC_NAME --as-root=true
oc debug dc $DC_NAME --node-name=$NODENAME
```

## **Build / Deploy**

| Command                               | Info                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| oc rollout                            | manage deployments from deployment configuration             |
| oc rollout latest                     | start a new deployment with the latest state                 |
| oc rollout undo                       | perform a rollback operation                                 |
| oc rollout history oc rollout history | - View historical information for a deployment configuration |
| oc rollout status                     | watch the status of a rollout until complete                 |
| oc tag                                | tag existing images into image streams                       |
| oc start-build                        | start a new build from a build configuration                 |
| oc cancel-build                       | cancel a build in progress                                   |
| oc import-image                       | pull in images and tags from an external Docker registry     |
| oc scale                              | change the number of pod replicas for a deployment           |

### Turn off/on DC triggers
```bash
# Turn off/on DC triggers to do a batch
# of changes without spam many deployments
oc rollout pause dc <dc name>
oc rollout resume dc <dc name>
```

## **Snippets**

### Run as root (anyuid)
```
oc create serviceaccount sa-anyuid
oc adm policy add-scc-to-user anyuid -z sa-anyuid
# create new-app before to get a dc
oc patch dc/deployment-config-name --patch '{"spec":{"template":{"spec":{"serviceAccountName": "sa-anyuid"}}}}'
```

### Run as root (anyuid) for every pod in project
```
oc adm policy add-scc-to-user anyuid -z default
```

### Docker push to ocp internal registry
```
oc extract -n default secrets/registry-certificates --keys=registry.crt
REGISTRY=$(oc get routes -n default docker-registry -o jsonpath='{.spec.host}')
mkdir -p /etc/containers/certs.d/${REGISTRY}
mv registry.crt /etc/containers/certs.d/${REGISTRY}/

oc adm new-project openshift-pipeline
oc create -n openshift-pipeline serviceaccount pipeline
SA_SECRET=$(oc get secret -n openshift-pipeline | grep pipeline-token | cut -d ' ' -f 1 | head -n 1)
SA_PASSWORD=$(oc get secret -n openshift-pipeline ${SA_SECRET} -o jsonpath='{.data.token}' | base64 -d)
oc adm policy add-cluster-role-to-user system:image-builder system:serviceaccount:openshift-pipeline:pipeline

docker login ${REGISTRY} -u unused -p ${SA_PASSWORD}
docker pull docker.io/library/hello-world
docker tag docker.io/library/hello-world ${REGISTRY}/openshift-pipeline/helloworld
docker push ${REGISTRY}/openshift-pipeline/helloworld

oc new-project demo-project
oc policy add-role-to-user system:image-puller system:serviceaccount:demo-project:default -n openshift-pipeline
oc new-app --image-stream=openshift-pipeline/helloworld:latest
```

### Create a configmap file and mount as a volume on DC
```
oc create configmap myconfigfile --from-file=./configfile.txt
oc set volumes dc/printenv --add --overwrite=true --name=config-volume --mount-path=/data -t configmap --configmap-name=myconfigfile
```

### Create Cron Job
```
oc run pi --image=perl --schedule='*/1 * * * *' \
    --restart=OnFailure --labels parent="cronjobpi" \
    --command -- perl -Mbignum=bpi -wle 'print bpi(2000)'
```
### Run Cron Job Manually
```
 oc create job --from=cronjob/mycronjob mycronjob-manual-001
```


### Delete pod Failed/Evicted/Pending
```
oc get pod --all-namespaces --field-selector 'status.phase==Failed' -o json | oc delete -f -
oc get pod --all-namespaces --field-selector 'status.phase==Evicted' -o json | oc delete -f -
oc get pod --all-namespaces --field-selector 'status.phase==Pending' -o json | oc delete -f -
```

### Reset Node
```bash
# Master Node: 
oc get nodes
oc adm manage-node <node_name> --schedulable=false
oc adm drain <node_name> --delete-local-data  --ignore-daemonsets

# Node:
systemctl stop docker origin-node.service 
# For Ocp Enterprise
# systemctl stop docker atomic-openshift-node

# Clean volumes ( Optional )
rm -rf /var/lib/origin/openshift.local.volumes
docker-storage-setup --reset
docker-storage-setup

# Restart
systemctl start docker origin-node.service
# For Ocp Enterprise
# systemctl stop docker atomic-openshift-node 

# Master node
# Make node schedulable
sudo oc adm manage-node <node_name> --schedulable=true
# check 
oc get node
```